package service

import (
	"exercise_crud/models"
	"exercise_crud/repository"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type ResponseModel struct {
	Code    int    `json: "code" validate: "required"`
	Message string `json: "code" validate: "required"`
}

// READ ALL
func ReadAllRole(c echo.Context) error {
	result := repository.ReadAllRole()
	return c.JSON(http.StatusOK, result)
}

// READ BY ID
func ReadIdRole(c echo.Context) error {
	id := c.QueryParam("roleId")
	data, _ := strconv.Atoi(id)
	result := repository.ReadIdRole(data)
	return c.JSON(http.StatusOK, result)
}

// CREATE
func CreateRole(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	U := new(models.RoleModel)
	if err := c.Bind(U); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.CreateRole(U))
	return c.JSON(http.StatusOK, Res)
}

// DELETE
func DeleteRole(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.QueryParam("roleId")
	data, _ := strconv.Atoi(id)
	Res = (*ResponseModel)(repository.DeleteRole(data))
	return c.JSON(http.StatusOK, Res)
}

// UPDATE
func UpdateRole(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.QueryParam("roleId")
	data, _ := strconv.Atoi(id)
	U := new(models.RoleModel)
	if err := c.Bind(U); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.UpdateRole(U, data))
	return c.JSON(http.StatusOK, Res)
}
