package routes

import (
	"exercise_crud/service"

	"github.com/labstack/echo"
)

func EndPoint() {
	e := echo.New()

	// ROLES ENDPOINT
	e.GET("role/readAll", service.ReadAllRole)
	e.GET("role/read", service.ReadIdRole)
	e.POST("role/create", service.CreateRole)
	e.PUT("role/update", service.UpdateRole)
	e.DELETE("role/delete", service.DeleteRole)

	e.Logger.Fatal(e.Start(":1323"))
}
