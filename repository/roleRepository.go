package repository

import (
	"exercise_crud/driver"
	"exercise_crud/models"
	"fmt"
)

// RESPONSE MODEL
type ResponseModel struct {
	Code    int    `json: "code" validate: "required"`
	Message string `json: "message" validate: "required"`
}

// CRUD

// READ ALL
func ReadAllRole() []models.RoleModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.RoleModel

	items, err := db.Query("SELECT role_id, role FROM tb_role")

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T\n", items)

	for items.Next() {
		var each = models.RoleModel{}
		var err = items.Scan(&each.RoleId, &each.Role)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)
	}

	if err = items.Err(); err != nil {
		fmt.Println(err.Error())
		return nil
	}

	return result
}

// READ BY ID
func ReadIdRole(roleId int) []models.RoleModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.RoleModel

	items, err := db.Query(`SELECT * FROM tb_role WHERE role_id = $1`, roleId)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T", items)

	for items.Next() {
		var each = models.RoleModel{}
		var err = items.Scan(&each.RoleId, &each.Role)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}
	}
	return result
}

// CREATE
func CreateRole(L *models.RoleModel) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	_, err = db.Exec(`INSERT INTO "tb_role" ("role") VALUES ($1)`, L.Role)

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Save Data"}
		return Res
	}

	fmt.Println("Insert Success!")
	Res = &ResponseModel{200, "Success Save Data"}
	return Res
}

// DELETE
func DeleteRole(roleId int) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	_, err = db.Exec(`DELETE FROM tb_role WHERE role_id = $1`, roleId)

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Delete Data"}
		return Res
	}

	fmt.Println("Delete Success!")
	Res = &ResponseModel{200, "Success Delete Data"}
	return Res
}

// UPDATE
func UpdateRole(L *models.RoleModel, roleid int) *ResponseModel {
	Res := &ResponseModel{500, "Internal Server Error"}

	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	_, err = db.Exec("UPDATE tb_role SET role = $1 WHERE role_id = $2", L.Role, roleid)

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "Failed to Update Data"}
		return Res
	}

	fmt.Println("Update Success!")
	Res = &ResponseModel{200, "Success Update Data"}
	return Res
}
